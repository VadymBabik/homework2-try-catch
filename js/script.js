"use strict";
const books = [
  {
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Воин-пророк",
  },
  {
    name: "Тысячекратная мысль",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70,
  },
  {
    author: "Дарья Донцова",
    name: "Детектив на диете",
    price: 40,
  },
  {
    author: "Дарья Донцова",
    name: "Дед Снегур и Морозочка",
  },
];

//* create elements
const divRoot = createHtmlElement("div", { id: "root" });
document.querySelector("body").prepend(divRoot);

//* functions
function createHtmlElement(tag, props) {
  const element = document.createElement(tag);
  if (props) {
    Object.keys(props).forEach((key) => (element[key] = props[key]));
  }
  return element;
}

function printItem(item, index) {
  const li = createHtmlElement("li");
  if (!item.author) {
    throw new Error(
      `index ${index}: author: error! missing author, name: ${item.name}, price: ${item.price}.`
    );
  }
  if (!item.name) {
    throw new Error(
      `index ${index}: author: ${item.author}, name: error! missing name, price: ${item.price}.`
    );
  }
  if (!item.price) {
    throw new Error(
      `index ${index}: author: ${item.author}, name: ${item.name}, price: error! missing price.`
    );
  }

  li.innerHTML = `index ${index} author: ${item.author}, name: ${item.name}, price: ${item.price}`;
  return li;
}

function printList(arr) {
  const ul = createHtmlElement("ul", { style: "list-style: none" });
  arr.map(function (item, index) {
    try {
      ul.append(printItem(item, index));
    } catch (error) {
      console.log(error.message);
    }
  });
  return ul;
}

const list = printList(books);
divRoot.append(list);
